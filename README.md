# Note
This repository is the **stable** version of MaLTPyNT. From now on, will only be used for bugfixes.
Please look at [HENDRICS](github.com/stingraysoftware/HENDRICS) to test the in-development future versions.

